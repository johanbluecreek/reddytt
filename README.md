# Reddytt

Do you also find yourself not being able to waste time on [reddit](https://www.reddit.com) efficiently enough? With this silly little script you can get rid off that unnecessary time spent on reading titles thinking "is this really the video I want to watch?", "is that image really worth my time?", or "I do not want to have to click for the next song!", clicking on links, clicking your way to the next page, etcetera, etcetera. `reddytt.py` will take care of that for you. It will start showing you videos, images, playing songs, taken from your favourite subreddit, just the first page or deeper if you want, until all links are consumed and you are faced with reality once again.

## Dependencies

Python modules: `argparse`, `requests` (rest should be built in); Player: `mpv`, downloader: `youtube-dl`

These dependencies can be installed using the command `pip install -r requirements.txt`, or possibly your system package manager.

### youtube-dl

The `youtube-dl` tool does not seem to be supported any longer (see e.g. [this comment on github](https://github.com/ytdl-org/youtube-dl/issues/30075#issuecomment-939196413)) and using `youtube-dl` may cause the download speed to be severely limited. Instead the tool [`yt-dlp`](https://github.com/yt-dlp/yt-dlp) can be used ([install instructions](https://github.com/yt-dlp/yt-dlp#installation)). If you only download the binaries, create a symbolic link called `youtube-dl` to point at the `yt-dlp` and reddytt should work as intended -- not all features has been tested yet. You may also want to look at [this issue in github](https://github.com/yt-dlp/yt-dlp/issues/142) that discussed the issue of streaming videos with `mpv` using `yt-dlp`, although `yt-dlp` appears to run out-of-the-box.

## Privacy notice

The Reddit API will only be called with the subreddit you provide to Reddytt. All other options you set will be handled locally on your machine by Reddytt and are *not* handled as calls to the Reddit API. All links parsed locally will then be handled by `mpv`, and may send it to whatever API that it needs to show the media.

## Usage

Short version, take a look at
```
$ ./reddytt.py -h
```

The long version,
```
$ ./reddytt.py [options] <subreddit> [-- [mpv-arguments]]
```
note that `<subreddit>` is just the name, e.g. `breadtube` not `r/breadtube` or anything else. The option available to you is for example `--depth d` which takes you `d` steps beyond the first page of the subreddit. Note that a negative depth means only already downloaded links will be played. All arguments following those are given to `mpv`.

Example,
```
$ ./reddytt.py --depth 1 breadtube -- --fs
```

All of the options are
```
usage: reddytt.py [options] <subreddit> [-- [opts]]

Play or download the links from your favourite subreddit.

positional arguments:
  subreddit             The subreddit you want to play (can be user name, see
                        `--type`).
  opts                  Options to pass to `mpv`, or `youtube-dl` if
                        `--download` is set.

optional arguments:
  -h, --help            show this help message and exit
  -d d, --depth d       How many pages into the subreddit you want to go. (`0`
                        is frontpage, each positive number another page after
                        that. Negative will not fetch new links at all.)
  -g, --gen-input       Trigger reddytt to generate reddytt's default
                        `input.conf`, and backup the old one. (It is good to
                        run this if you have updated reddytt.)
  -v, --version         Prints version number and exits.
  -s, --shuffle         Shuffles the order of the links.
  -f, --fullscreen      Enables full-screen (mpv).
  -m, --mute            Starts playing muted.
  -a, --audio           Downloads audio only (youtube-dl).
  -n nsfw, --nsfw nsfw  Default is to block all nsfw content (`-n 0`). `nsfw >
                        0` makes it available, `nsfw < 0` makes it exclusively
                        nsfw.
  -k keywords, --keywords keywords
                        Comma-separated list of keywords to have in title (of
                        the Reddit post).
  -l length, --length length
                        Comma-separated times of minimal and maximum time of
                        link to be played. For example `-l 5:00,10:00` plays
                        videos between 5 and 10 minutes. Hours is the highest
                        unit, with no limit.
  -b ban, --ban ban     Comma-separated list of subreddits to ban (will
                        omitted from being displayed).
  -u upvotes, --upvotes upvotes
                        Set a number for lowest allowed net up-votes, default
                        is to only show positives (`-u 1`).
  --gore                Allow more gore oriented subreddits (not allowed by
                        default).
  --type type           Switch what 'type' of posts to play; 'sub' plays
                        subreddit posts (default), 'user' plays posts by user
  -r, --radio           Enters into radio-mode: Links are shuffled, links will
                        be re-run next time, and video output is disabled.
                        This is for listening through a music-centered
                        subreddit.
  -p, --presentation    Enters into presentation-mode: Images and
                        videos/animations are looped, and made full-screen.
  -i, --interactive     Enters into interactive-mode: A list of titles will be
                        printed for the user to select.
  -t, --test            Enters into test-mode: A temporary directory is set up
                        instead of `.reddytt/[subreddit]` which is removed on
                        exit.
  --download            Enters into download-mode: Links will not be passed to
                        mpv for viewing/listening but instead to youtube-dl
                        for download. Files will be stored in
                        `.reddytt/[subreddit]/downloads/`.
  --search              Search for a subreddit.
  --keymap              Displays kep mapping (by showing input file) and
                        exits.
```


### Tips and tricks

While Reddytt was designed with videos in mind, it has some extra modes.

#### Radio-mode

Turn Reddytt into a music-player for your music related subreddit
```
$ ./reddytt.py --radio --depth 2 breadtunes
```

#### Presentation-mode

For your image related subreddit
```
$ ./reddytt.py --presentation all
```
this will show a loop of the video and/or image until you select the next one (`>`).

#### Interactive-mode

You can enter interactive mode with
```
$ ./reddytt.py --interactive breadtube
```
where Reddytt will display a list of all the links and you have to manually enter which one to show. The key bindings and behaviour is mostly the same. Some differences and unexpected behaviour may be that the keys `q` and `>` does the same (exits `mpv` and displays list of links again.), and links are automatically removed from the display list once they have finished (unless exited with `Ctrl+h`).

#### Download-mode

If you are going away and won't have internet, prepare for your trip by downloading the videos
```
$ ./reddytt.py --download breadtube
```
of your music
```
$ ./reddytt.py --download --audio breadtunes
```
to have on another device, which makes the downloaded files available in `~/.reddytt/[subreddit]/downloads/`.

#### Test-mode

By running the test-mode
```
$ ./reddytt.py --test all
```
nothing of your session will be stored in `.reddytt/` when you are done.

### Key-mapping

Reddytt will generate a `input.conf`-file (to be stored in `~/.reddytt/`) and override the `mpv` default, or user-set, key mapping (that is, reddytt runs `mpv` with `--input-conf=` set). Nothing will permanently change for you, but you should be aware of the default key-mapping of reddytt:

 * `q`: Saves and (q)uits remaining links
 * `>`: Plays next video
 * `R`: (R)emembers video link (and title) in `~/.reddytt/remember` (plain text)
 * `i`: Prints the (i)nfo (Reddit-given title of the video) in `mpv`
 * `Ctrl+o`: Video link is (o)pened in default browser (using `xdg-open`)
 * `Ctrl+r`: Opens (r)eddit comments link in default browser
 * `n`: Displays (n)umber of videos left in the list
 * `Ctrl+h`: Quits playing the link but (h)olds-on to the link, so that when running Reddytt again, it will be replayed. For the "I don't want to watch this now but definitely later"-videos
 * `Ctrl+d`: Quits playing the link and (d)ownloads it instead to `~/.reddytt/[subreddit]/downloads/`
 * `Ctrl+l`: Quits playing the link and (l)oops back to it again. This is useful if the video or audio crashes during playback (which can happen on bad connections), to restart the playback and hopefully restore video or audio
 * `Ctrl+x`: Quits playing the link and e(x)cludes the link into the future. This is useful when running a subreddit in radio-mode; if you want to exclude something from the radio "broadcast" in the future, hit `Ctrl+x` instead of `>`. Excludes can be cleared by removing `~/.reddytt/[subreddit]/exclude`.

To override, edit `~/.reddytt/input.conf` at your own leisure (note that the `n` and `Ctrl+r` bindings are hard-coded).

On quit, it saves seen videos to `~/.reddytt/[subreddit]/seen` and a list of unseen videos to `~/.reddytt/[subreddit]/unseen` (using pickle).

Playback may get stuck for various reasons, or next video may not show up. If this happens: `Ctrl+l` to retry to play the video in place, or `Ctrl+h` to hold on to it until the next time you run reddytt. If that fails, exiting with `q` will leave the link in the list of links to still be shown, and running reddytt again will eventually re-encounter the link and will be attempted to be played again. If the link is still not playing as it should, and it keeps getting stuck, then `>` will remove it from the links to be played.

### Updates

If you update reddytt, meaning you run reddytt and it may not be the same version as the one that created the files in `~/.reddytt/`, then it might be good to run
```
$ ./reddytt.py --gen-input
```
to get the latest input file so that the key maps work as intended.

## TODO

* Add travel functionality. Set a number, and download that many videos that is not in seen. Have it also so that one does not have to watch them using external player, but if files are found then those are played, so that seen/unseen and such can still be used.
* Interactive-mode
  - Remove items without opening them in `mpv` (perhaps make default behaviour instead of automatically dropping?)
  - List times along with titles as well?
  - Drop to normal reddytt-mode
  - Add switches to turn on and off features
  - Make a printable help menu instead of displaying it at the input text
