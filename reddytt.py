#!/usr/bin/env python3

################################################################################
            ######  ####### ######  ######  #     # ####### #######
            #     # #       #     # #     #  #   #     #       #
            #     # #       #     # #     #   # #      #       #
   #####    ######  #####   #     # #     #    #       #       #       #####
            #   #   #       #     # #     #    #       #       #
            #    #  #       #     # #     #    #       #       #
            #     # ####### ######  ######     #       #       #
################################################################################
#
#   reddytt.py
#   https://gitlab.com/johanbluecreek/reddytt
#
__version__ = "1.8.2"
user_agent = "Reddytt v{}".format(__version__)
#
################################################################################
################################################################################

################################################################################
              ### #     # ######  ####### ######  #######  #####
               #  ##   ## #     # #     # #     #    #    #     #
               #  # # # # #     # #     # #     #    #    #
               #  #  #  # ######  #     # ######     #     #####
               #  #     # #       #     # #   #      #          #
               #  #     # #       #     # #    #     #    #     #
              ### #     # #       ####### #     #    #     #####
################################################################################

import os
import pickle
import requests
import json
import re
import subprocess
import sys
import argparse as ap
import copy
from datetime import date
import time
import random
from shutil import copyfile, rmtree

################################################################################
      ####### #     # #     #  #####  ####### ### ####### #     #  #####
      #       #     # ##    # #     #    #     #  #     # ##    # #     #
      #       #     # # #   # #          #     #  #     # # #   # #
      #####   #     # #  #  # #          #     #  #     # #  #  #  #####
      #       #     # #   # # #          #     #  #     # #   # #       #
      #       #     # #    ## #     #    #     #  #     # #    ## #     #
      #        #####  #     #  #####     #    ### ####### #     #  #####
################################################################################

class video_time():

    def __init__(self, link):
        self.s = None
        self.m = None
        self.h = None
        self.set_attributes(link)

    def __lt__(self, other):
        if self._to_seconds() < other._to_seconds():
            return True
        return False

    def __gt__(self, other):
        return other < self

    def __repr__(self, ):
        return f'{self.h}:{self.m}:{self.s}'

    def set_attributes(self, link):
        if re.match('[a-z]', link):
            # looks like proper link
            times = self._get_time(link)
        else:
            # we assume it is just a time
            times = self._parse_time(link)
        attrs = ['self.s', 'self.m', 'self.h']
        for t in range(3):
            if t < len(times):
                exec(attrs[t] + f' = {times[t]}')
            else:
                exec(attrs[t] + f' = 0')

    def _parse_time(self, time_string):
        w = time_string.split(':')
        w = [ int(t) for t in w[::-1] ]
        return w

    def _get_time(self, playlink):
        output = subprocess.Popen( [
            'youtube-dl',
            '--get-duration',
            playlink
        ], stdout=subprocess.PIPE ).communicate()[0]
        output = output.decode().split()[0]
        return self._parse_time(output)

    def _to_seconds(self):
        return 60*60*self.h + 60*self.m + self.s

class dummy_process():

    def __init__(self, code):
        self.returncode = code

    def communicate(self):
        pass

 ####  #####  ######   ##   ##### ######         # #    # #####  #    # #####
#    # #    # #       #  #    #   #              # ##   # #    # #    #   #
#      #    # #####  #    #   #   #####          # # #  # #    # #    #   #
#      #####  #      ######   #   #              # #  # # #####  #    #   #
#    # #   #  #      #    #   #   #              # #   ## #      #    #   #
 ####  #    # ###### #    #   #   ######         # #    # #       ####    #
                                         #######

def create_input(work_dir):
    """
        create_input(work_dir)

    Create `mpv` `input.conf`-file to overide default settings.
    """
    # Create path to file
    input_file = work_dir + "/input.conf"
    # Make a backup if relevant
    if os.path.isfile(input_file):
        backup_file = input_file + "-" + date.today().isoformat() + "-" + str(int(time.time()))
        print("Reddytt: Creating backup of old `input.conf`: %s" % backup_file)
        copyfile(input_file, backup_file)

    with open(input_file, 'w') as f:
        f.write('# Next:\n')
        f.write('> quit 0\n')

        f.write('# Quit:\n')
        f.write('q quit 4\n')

        f.write('# Remember:\n')
        f.write('R run "/bin/bash" "-c" "echo \\\"${title}: ${path}\\\" >> ~/.reddytt/remember"\n')

        f.write('# Display info:\n')
        f.write('i show-text "${title}"\n')

        f.write('# Open youtube link:\n')
        f.write('Ctrl+o run "/bin/bash" "-c" "xdg-open \\\"${path}\\\""\n')
        f.write('0xf run "/bin/bash" "-c" "xdg-open \\\"${path}\\\""\n')    # from cmd

        f.write('# Hold on to link for later:\n')
        f.write('Ctrl+h quit 5\n')

        f.write('# Download:\n')
        f.write('Ctrl+d quit 6\n')
        f.write('0x4 quit 6\n')     # from cmd

        f.write('# Loop link to restart:\n')
        f.write('Ctrl+l quit 2\n')
        f.write('0xc quit 2\n')     # from cmd

        f.write('# Exclude link:\n')
        f.write('Ctrl+x quit 7\n')
        f.write('0x18 quit 7\n')    # from cmd

        f.write('# Also:\n')
        f.write('# "n" shows number of links left\n')
        f.write('# "Ctrl+r" opens reddit link\n')


##### #    # #####          # #    # #####  #    # #####
  #   ##  ## #    #         # ##   # #    # #    #   #
  #   # ## # #    #         # # #  # #    # #    #   #
  #   #    # #####          # #  # # #####  #    #   #
  #   #    # #              # #   ## #      #    #   #
  #   #    # #              # #    # #       ####    #
                    #######

def tmp_input(work_dir, link, num):
    """
        tmp_input(work_dir, link, num)

    Generates temporary `input.conf` from the base one.
    """
    # Create paths to files
    input_file = work_dir + "/input.conf"
    tmp_file = work_dir + "/input.conf_tmp"

    # Copy the base
    copyfile(input_file, tmp_file)

    # Add the extra mappings
    # Map 'Ctrl+r' to open Reddit-link in browser
    crtlr_string = ""
    try:
        crtlr_string = 'Ctrl+r run "/bin/bash" "-c" "xdg-open \\\"{}{}\\\""\n'.format("https://www.reddit.com", link[2])
        cmd_string = '0x12 run "/bin/bash" "-c" "xdg-open \\\"{}{}\\\""\n'.format("https://www.reddit.com", link[2])      # from cmd
    except IndexError:
        print("Reddytt: Old link encountered.")
        crtlr_string = 'Ctrl+r show-text "Reddytt: Reddit link not available."\n'
        cmd_string = '0x12 show-text "Reddytt: Reddit link not available."\n'      # from cmd

    # Map 'n' to show links-left number
    n_string = 'n show-text "{}"\n'.format(str(num))

    with open(tmp_file, 'a') as f:
        f.write(crtlr_string)
        f.write(cmd_string)
        f.write(n_string)

 ####  #      ######   ##   #    #         #   # #####
#    # #      #       #  #  ##   #          # #    #
#      #      #####  #    # # #  #           #     #
#      #      #      ###### #  # #           #     #
#    # #      #      #    # #   ##           #     #
 ####  ###### ###### #    # #    #           #     #
                                   #######

def clean_yt(link_list):
    """
        clean_yt(link_list)

    Cleans up all youtube-links.
    """
    # List to return
    new_list = []
    for link in link_list:
        # Only handle youtube links here
        if re.match(r"^https://www\.youtube\.com/watch", link[0]):
            # Find the label
            videolabel = re.search('v=([^&?]*)', link[0]).group(1)
            # If there for some strange reason would not be one
            if videolabel is None:
                print('Reddytt: skipping URL without video label:', link)
                continue
            # Add the cleaned up link
            try:
                new_list.append(('https://www.youtube.com/watch?v=' + videolabel, link[1], link[2]))
            except IndexError:
                new_list.append(('https://www.youtube.com/watch?v=' + videolabel, link[1]))
        else:
            # Or just add the original link if not youtube
            new_list.append(link)

    return new_list

#  ####  #####    ##   #    # #    # ###### #####
# #      #    #  #  #  ##   # ##   # #      #    #
#  ####  #####  #    # # #  # # #  # #####  #    #
#      # #    # ###### #  # # #  # # #      #    #
# #    # #    # #    # #   ## #   ## #      #    #
#  ####  #####  #    # #    # #    # ###### #####

def isbanned(subreddit, ban = []):
    banned_list = [
        'The_Donald'
    ] + ban
    banned_list = [b.lower() for b in banned_list]
    sr = subreddit.lower()
    return sr in banned_list

#  ####   ####   ####  #####  ######
# #      #    # #    # #    # #
#  ####  #      #    # #    # #####
#      # #  ### #    # #####  #
# #    # #    # #    # #   #  #
#  ####   ####   ####  #    # ######


def isgore(subreddit):
    """
        isgore(subreddit)

    Answers if a subreddit may contain gore.
    """
    gore_list = [
        'holdmyfeedingtube',
        'medizzy',
        'CatastroficFailure',
        'MMA',
        'MorbidReality',
        'CombatFootage',
        'FiftyFifty',
        'darkstockphotos',
        'MedicalGore',
        'cursedimages'
    ]
    return subreddit in gore_list

 ####  ###### #####          ####  #    # #####
#    # #        #           #      #    # #    #
#      #####    #            ####  #    # #####
#  ### #        #                # #    # #    #
#    # #        #           #    # #    # #    #
 ####  ######   #            ####   ####  #####
                    #######

def get_sub(subreddit):
    """
        get_sub(subreddit)

    Returns input unless input is 'random' or a sub that is banned, for which
    it will instead return a random subreddit or exit.
    """

    if isbanned(subreddit):
        print("Reddytt: Subreddit is banned. Exiting.")
        sys.exit()

    if subreddit.lower() == "random":
        link = "https://reddit.com/random/.json"
        ua = user_agent + " " + str(random.randint(100,999))
        req = requests.get(link, headers={'User-Agent': ua})
        data = req.json()

        return data[0]['data']['children'][0]['data']['subreddit']

    return subreddit

#####   ####           ####  ######   ##   #####   ####  #    #
#    # #    #         #      #       #  #  #    # #    # #    #
#    # #    #          ####  #####  #    # #    # #      ######
#    # #    #              # #      ###### #####  #      #    #
#    # #    #         #    # #      #    # #   #  #    # #    #
#####   ####           ####  ###### #    # #    #  ####  #    #
              #######


def do_search(subreddit):
    """
        do_search(subreddit)

    Prints subreddit search results.
    """
    link = "https://reddit.com/search/.json?q=" + subreddit + "&type=sr"
    ua = user_agent + " " + str(random.randint(100,999))
    req = requests.get(link, headers={'User-Agent': ua})
    data = req.json()

    for child in data['data']['children']:
        print(" --- " + child['data']['display_name'] + " --- ")
        print("Description: " + child['data']['public_description'])
        print("")

    sys.exit()


#####  ######  ####  #      # #    # #    #  ####
#    # #      #    # #      # ##   # #   #  #
#    # #####  #    # #      # # #  # ####    ####
#####  #      #  # # #      # #  # # #  #        #
#   #  #      #   #  #      # #   ## #   #  #    #
#    # ######  ### # ###### # #    # #    #  ####

def reqlinks(subreddit, nsfw, keywords, ban, upvotes, gore, post_type, after='', sr=''):
    """
        reqlinks(subreddit, nsfw, keywords, ban, upvotes, gore, post_type, after='', sr='')

    Request and parse out Reddytt-supported links at `link`.
    """

    if not sr:
        sr = get_sub(subreddit)

    link = "https://reddit.com/"
    if post_type == 'user':
        link += "u/" + sr + "/.json"
    else:
        link += "r/" + sr + "/.json"
    if after:
        link = link + "?after={}".format(after)

    # Generate a seed for the user agent and send request
    ua = user_agent + " " + str(random.randint(100,999))
    req = requests.get(link, headers={'User-Agent': ua})
    data = req.json()

    # Handle errors reddit might give us
    if 'error' in data.keys():
        print("Reddytt: Was presented with the Reddit error: " + str(data['error']) + " -- " + data['message'])
        print("Reddytt: Depending on the error, wait a while and try again later, or file an issue with Reddytt.")
        sys.exit()

    # Parse out all children
    ## Subject to 'nsfw' flag
    children = data['data']['children']
    if post_type == 'user':
        children = [ child for child in children if child['kind'] == 't3' ]
    if nsfw > 0:
        # nsfw > 0: Mixed
        children = [child for child in children if not child['data']['is_self']]
    elif nsfw < 0:
        # nsfw < 0: NSFW
        children = [child for child in children if (not child['data']['is_self']) and (child['data']['over_18'])]
    else:
        # nsfw == 0: SFW
        children = [child for child in children if (not child['data']['is_self']) and (not child['data']['over_18'])]
    ## Subject to keywords
    children = [child for child in children if any([ kw in child['data']['title'].lower() for kw in keywords])]
    ## Subject to ban
    children = [child for child in children if not isbanned(child['data']['subreddit'], ban)]
    ## Subject to upvote limit
    children = [child for child in children if child['data']['score'] >= upvotes ]
    ## Subject to gore flag
    if not gore:
        # If the subreddit may potentially contain gore, then it should have
        # the 'over_18' true, so only then we remove these subreddits.
        children = [child for child in children if not (isgore(child['data']['subreddit']) and child['data']['over_18'])]

    # Collect video urls and titles
    links = []
    if subreddit == 'all' or subreddit.lower() == 'random':
        links = [
            (child['data']['url'], child['data']['subreddit'] + ": " + child['data']['title'], child['data']['permalink'])
        for child in children]
    else:
        links = [
            (child['data']['url'], child['data']['title'], child['data']['permalink'])
        for child in children]

    # Clean up links
    links = clean_yt(links)

    # Find the 'after' variable
    after = data['data']['after']

    return links, after, sr

################################################################################
                          #     #    #    ### #     #
                          ##   ##   # #    #  ##    #
                          # # # #  #   #   #  # #   #
                          #  #  # #     #  #  #  #  #
                          #     # #######  #  #   # #
                          #     # #     #  #  #    ##
                          #     # #     # ### #     #
################################################################################

if __name__ == '__main__':

            #
            #      ##   #####   ####  #    # #    # ###### #    # #####  ####
            #     #  #  #    # #    # #    # ##  ## #      ##   #   #   #
                 #    # #    # #      #    # # ## # #####  # #  #   #    ####
            #    ###### #####  #  ### #    # #    # #      #  # #   #        #
            #    #    # #   #  #    # #    # #    # #      #   ##   #   #    #
            #    #    # #    #  ####   ####  #    # ###### #    #   #    ####

    ### Resolve arguments ###

    parser = ap.ArgumentParser(usage='%(prog)s [options] <subreddit> [-- [opts]]', description='Play or download the links from your favourite subreddit.')

    # Optional arguments
    parser.add_argument('-d', '--depth', metavar='d', type=int, default=0, help='How many pages into the subreddit you want to go. (`0` is frontpage, each positive number another page after that. Negative will not fetch new links at all.)')
    parser.add_argument('-g', '--gen-input', action='store_true', help='Trigger reddytt to generate reddytt\'s default `input.conf`, and backup the old one. (It is good to run this if you have updated reddytt.)')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__), help="Prints version number and exits.")
    parser.add_argument('-s', '--shuffle', action='store_true', help="Shuffles the order of the links.")
    parser.add_argument('-f', '--fullscreen', action='store_true', help="Enables full-screen (mpv).")
    parser.add_argument('-m', '--mute', action='store_true', help="Starts playing muted.")
    parser.add_argument('-a', '--audio', action='store_true', help="Downloads audio only (youtube-dl).")
    parser.add_argument('-n', '--nsfw', metavar='nsfw', type=int, default=0, help="Default is to block all nsfw content (`-n 0`). `nsfw > 0` makes it available, `nsfw < 0` makes it exclusively nsfw.")
    parser.add_argument('-k', '--keywords', metavar='keywords', type=str, default='', help="Comma-separated list of keywords to have in title (of the Reddit post).")
    parser.add_argument('-l', '--length', metavar='length', type=str, default='', help="Comma-separated times of minimal and maximum time of link to be played. For example `-l 5:00,10:00` plays videos between 5 and 10 minutes. Hours is the highest unit, with no limit.")
    parser.add_argument('-b', '--ban', metavar='ban', type=str, default='', help="Comma-separated list of subreddits to ban (will omitted from being displayed).")
    parser.add_argument('-u', '--upvotes', metavar='upvotes', type=int, default=1, help="Set a number for lowest allowed net up-votes, default is to only show positives (`-u 1`).")
    parser.add_argument('--gore', action='store_true', help="Allow more gore oriented subreddits (not allowed by default).")
    parser.add_argument('--type', metavar='type', type=str, default='sub', help="Switch what 'type' of posts to play; 'sub' plays subreddit posts (default), 'user' plays posts by user")


    ## Alternative play-modes
    parser.add_argument('-r', '--radio', action='store_true', help='Enters into radio-mode: Links are shuffled, links will be re-run next time, and video output is disabled. This is for listening through a music-centered subreddit.')
    parser.add_argument('-p', '--presentation', action='store_true', help='Enters into presentation-mode: Images and videos/animations are looped, and made full-screen.')
    parser.add_argument('-i', '--interactive', action='store_true', help='Enters into interactive-mode: A list of titles will be printed for the user to select.')
    parser.add_argument('-q', '--quit', action='store_true', help="Quit instead of playing anything. This will cause the program to fetch links for future viewing, but not actually play them.")
    parser.add_argument('-t', '--test', action='store_true', help='Enters into test-mode: A temporary directory is set up instead of `.reddytt/[subreddit]` which is removed on exit.')
    parser.add_argument('--download', action='store_true', help='Enters into download-mode: Links will not be passed to mpv for viewing/listening but instead to youtube-dl for download. Files will be stored in `.reddytt/[subreddit]/downloads/`.')
    parser.add_argument('--search', action='store_true', help='Search for a subreddit.')
    parser.add_argument('--keymap', action='store_true', help='Displays kep mapping (by showing input file) and exits.')


    # Positional arguments
    parser.add_argument('subreddit', type=str, help='The subreddit you want to play (can be user name, see `--type`).', nargs='?')
    parser.add_argument('opts', nargs=ap.REMAINDER, help='Options to pass to `mpv`, or `youtube-dl` if `--download` is set.')

    args = parser.parse_args()

    subreddit = args.subreddit

    depth = args.depth
    nsfw = args.nsfw
    keywords = args.keywords.split(',')
    keywords = [kw.lower() for kw in keywords]
    length = [ video_time(t) for t in args.length.split(',') if t != '']
    ban = args.ban.split(',')
    upvotes = args.upvotes
    gore = args.gore
    post_type = args.type
    gen_input = args.gen_input
    shuffle = args.shuffle

    download_mode = args.download

    if download_mode:
        mpv_args = []
        ydl_args = args.opts
    else:
        mpv_args = args.opts
        ydl_args = []

    if args.fullscreen:
        mpv_args += ['--fs']

    if args.mute:
        mpv_args += ['--mute=yes']

    if args.audio:
        ydl_args += ['-x']

    # Modes:
    quit_mode = args.quit

    radio_mode = args.radio
    if radio_mode:
        mpv_args += ['--no-video']

    presentation_mode = args.presentation
    if presentation_mode:
        mpv_args += ['--image-display-duration=inf', '--loop=inf', '--fs']

    interactive_mode = args.interactive

    test_mode = args.test

    search_mode = args.search
    if search_mode:
        do_search(subreddit)

    keymap = args.keymap

    # Check arguments for incompatibilities
    if subreddit == None and not (gen_input or keymap):
        print("Reddytt: No subreddit given, exiting. Try `reddytt --help`.")
        sys.exit()

    if not subreddit == None:
        if "?" in subreddit:
            print("Reddytt: Are you trying to manipulate the Reddit-api call? Stop that!")
            sys.exit()

    if not subreddit == None:
        # All modes are not compatible...
        if download_mode and test_mode:
            print("Reddytt: I don't think thats right; you are asking to download files and then remove them. Exiting.")
            sys.exit()
        if radio_mode and presentation_mode:
            print("Reddytt: Entering annoying repeat-mode.")


            #                                       ##
            #    ###### # #      ######  ####      #  #      #####  # #####
            #    #      # #      #      #           ##       #    # # #    #
                 #####  # #      #####   ####      ###       #    # # #    #
            #    #      # #      #           #    #   # #    #    # # #####
            #    #      # #      #      #    #    #    #     #    # # #   #
            #    #      # ###### ######  ####      ###  #    #####  # #    #

    ### Check on directories and files ###

    # Setup working directory
    work_dir = os.environ['HOME'] + "/.reddytt"
    if not os.path.isdir(work_dir):
        os.mkdir(work_dir)
    # Change working directory if in test-mode
    if test_mode:
        if os.path.isfile(work_dir + "/input.conf"):
            input_file = work_dir + "/input.conf"
        work_dir += "/test_mode"
        if os.path.isdir(work_dir):
            print("Reddytt: Old `test_mode` folder was not removed, when it should have been. Removing now and recreating.")
            rmtree(work_dir)
        os.mkdir(work_dir)
        # Make sure user-set input configuration is preserved
        copyfile(input_file, work_dir + "/input.conf")


    # New optional flag triggers input.conf generation, or if the file does not exists.
    if gen_input:
        print("Reddytt: Generating 'input.conf' file and exiting.")
        if not os.path.isdir(work_dir):
            os.mkdir(work_dir)
        create_input(work_dir)
        sys.exit()
    elif not os.path.isfile(work_dir + "/input.conf"):
        print("Reddytt: No input-file found, creating 'input.conf' file.")
        create_input(work_dir)


    sr_dir = work_dir + "/%s" % subreddit
    # File for seen videos
    seen_file = sr_dir + "/seen"
    seen_links = []
    # File for unseen videos
    unseen_file = sr_dir + "/unseen"
    unseen_links = []
    # File for links to be excluded
    exclude_file = sr_dir + "/exclude"
    exclude_links = []
    # File for overriding mpv input.conf
    input_file = work_dir + "/input.conf"
    tmp_input_file = work_dir + "/input.conf_tmp"
    # File for remembering links
    remember_file = work_dir + "/remember"
    # Download directory
    dl_dir = sr_dir + "/downloads"

    # We now happen to have `input_file` defined so if `keymap` is set
    # we can print and exit
    if keymap:
        with open(input_file, 'r') as inf:
            print(inf.read())
        sys.exit()


    if not os.path.isdir(work_dir):
        print("Reddytt: Working directory not found. Creating %s, and files." % work_dir)
        os.mkdir(work_dir)
        os.mkdir(sr_dir)
        os.mkdir(dl_dir)
        os.system("touch %s" % seen_file)
        with open(seen_file, 'wb') as f:
            pickle.dump(seen_links, f)
        os.system("touch %s" % unseen_file)
        with open(unseen_file, 'wb') as f:
            pickle.dump(unseen_links, f)
        os.system("touch %s" % exclude_file)
        with open(exclude_file, 'wb') as f:
            pickle.dump(exclude_links, f)
    elif not os.path.isdir(sr_dir):
        print("Reddytt: Working directory found, but no subreddit directory. Creating %s, and files." % sr_dir)
        os.mkdir(sr_dir)
        os.mkdir(dl_dir)
        os.system("touch %s" % seen_file)
        with open(seen_file, 'wb') as f:
            pickle.dump(seen_links, f)
        os.system("touch %s" % unseen_file)
        with open(unseen_file, 'wb') as f:
            pickle.dump(unseen_links, f)
        os.system("touch %s" % exclude_file)
        with open(exclude_file, 'wb') as f:
            pickle.dump(exclude_links, f)
    else:
        print("Reddytt: Working directory found. Loading variables.")

        try:
            with open(seen_file, 'rb') as f:
                seen_links = pickle.load(f)
        except FileNotFoundError:
            # This allows you to remove the file manually to reset.
            print("Reddytt: (Seen) File not found. Creating empty file.")
            os.system("touch %s" % seen_file)

        try:
            with open(unseen_file, 'rb') as f:
                unseen_links = pickle.load(f)
        except FileNotFoundError:
            # This allows you to remove the file manually to reset.
            print("Reddytt: (Unseen) File not found. Creating empty file.")
            os.system("touch %s" % unseen_file)

        try:
            with open(exclude_file, 'rb') as f:
                exclude_links = pickle.load(f)
        except FileNotFoundError:
            # This allows you to remove the file manually to reset.
            print("Reddytt: (Exclude) File not found. Creating dummy file.")
            os.system("touch %s" % exclude_file)
            with open(exclude_file, 'wb') as f:
                pickle.dump(exclude_links, f)

        # Resolve compatability issues with older versions:
        seen_links = [ (l, '') if not type(l) == tuple else l for l in seen_links]
        unseen_links = [ (l, '') if not type(l) == tuple else l for l in unseen_links]

    # User may have provided keywords, that the unseen_links should respect
    excluded_links = []
    unseen_links_new = []
    for ul in unseen_links:
        if any([kw in ul[1].lower() for kw in keywords]):
            unseen_links_new.append(ul)
        else:
            # Links to be saved if keywords have been changed in the future
            excluded_links.append(ul)
    unseen_links = unseen_links_new

            #
            #     ####  ###### #####    #      # #    # #    #  ####
            #    #    # #        #      #      # ##   # #   #  #
                 #      #####    #      #      # # #  # ####    ####
            #    #  ### #        #      #      # #  # # #  #        #
            #    #    # #        #      #      # #   ## #   #  #    #
            #     ####  ######   #      ###### # #    # #    #  ####

    ### Get links to play ###

    new_links = []

    if depth < 0:
        # Just a warning. Negative means not fetching new links.
        print("Reddytt: Depth set to negative. No new links will be fetched.")
    else:
        # Otherwise, proceed to get links.
        print("Reddytt: Fetching links.")
        new_links, after, sr = reqlinks(subreddit, nsfw, keywords, ban, upvotes, gore, post_type)

    # Go deeper
    d = 0
    while depth > d and not after == None:

        newer_links, after, sr = reqlinks(subreddit, nsfw, keywords, ban, upvotes, gore, post_type, after, sr)
        d += 1

        new_links += newer_links
        new_links = list(set(new_links))

    # We also want to watch the stored ones
    watch_links = new_links + unseen_links
    # Remove repeted entries of links as well as the ones already seen (unless in radio-mode)
    if not radio_mode:
        watch_links = list(set(watch_links)-set(seen_links))
    else:
        # If in radio_mode, we want to listen to all of it again
        watch_links = list(set(watch_links + seen_links))

        # The above does not work as intended... lets remove all overlaps
        nw_links = []
        for l in range(len(watch_links)):
            if not watch_links[l][0] in map(lambda x: x[0][0], nw_links):
                nw_links.append((watch_links[l], l))
        watch_links = list(map(lambda x: watch_links[x[1]], nw_links))


    # If we have excluded links we should remove them
    watch_links = [ wl for wl in watch_links if wl[0] not in exclude_links ]

    print("Reddytt: Links to watch: %i" % len(watch_links))

            #
            #     ####  #####   ##   #####  #####
            #    #        #    #  #  #    #   #
                  ####    #   #    # #    #   #
            #         #   #   ###### #####    #
            #    #    #   #   #    # #   #    #
            #     ####    #   #    # #    #   #

    ### Start watching ###

    print("Reddytt: The watch begins.")
    print("")

    if radio_mode or shuffle:
        random.shuffle(watch_links)

    save_links = copy.copy(watch_links)

    # Quit mode

    if quit_mode:
        print("Reddytt: No playing, quitting instead.")
        with open(seen_file, 'wb') as f:
            pickle.dump(seen_links, f)
        with open(unseen_file, 'wb') as f:
            tosave_links = excluded_links + save_links
            pickle.dump(tosave_links, f)
        with open(exclude_file, 'wb') as f:
            pickle.dump(exclude_links, f)
        sys.exit()

    # Interactive mode play

    if interactive_mode:

        exit = False

        next = 0

        while not exit:

            if len(save_links) == 0:
                print("Reddytt: No more links. Exiting.")
                break

            if len(save_links) <= 10 and len(save_links) > 0:
                for link in save_links:
                    print(f"{save_links.index(link)+1}:\t\"{link[1]}\" \n \t({link[0]})\n")
                answer = input("\nSelect what to play ('q' to quit): ")
            elif len(save_links) > 10 and next == 0:
                for link in save_links[0:10]:
                    print(f"{save_links.index(link)+1}:\t\"{link[1]}\" \n \t({link[0]})\n")
                answer = input("\nSelect what to play ('n' for next page, 'q' to quit): ")
            elif len(save_links) <= 10 + 10*next and next > 0:
                for link in save_links[10*next:len(save_links)]:
                    print(f"{save_links.index(link)+1}:\t\"{link[1]}\" \n \t({link[0]})\n")
                answer = input("\nSelect what to play ('p' for previous page, 'q' to quit): ")
            elif len(save_links) > 10 + 10*next and next > 0:
                for link in save_links[10*next:10 + 10*next]:
                    print(f"{save_links.index(link)+1}:\t\"{link[1]}\" \n \t({link[0]})\n")
                answer = input("\nSelect what to play ('n' for next page, 'p' for previous page, 'q' to quit): ")
            elif len(save_links) == 0:
                print("Reddytt: No links left. Exiting.")
                break
            else:
                print("Reddytt: Not sure what happened...")
                next = 0
                continue

            if answer == 'q':
                break
            elif answer == 'n' and len(save_links) > 10+10*next:
                next += 1
                continue
            elif answer == 'n' and len(save_links) <= 10+10*next:
                print("\nReddytt: No next page.\n")
                continue
            elif answer == 'p' and next == 0:
                print("\nReddytt: No previous page.\n")
                continue
            elif answer == 'p' and next > 0:
                next -= 1
                continue

            try:
                answer = int(answer)-1
                link = save_links[answer]
            except:
                print("\nReddytt: Input not understood or out of bounds. Try again.\n")
                continue

            # Prepare to play!
            ## fix tmp_input
            tmp_input(work_dir, link, len(save_links))
            ## Prepare title and link
            playtitle = link[1].replace('"',"'")
            playlink = link[0]

            ## Play
            p = subprocess.Popen(
                [
                    'mpv',
                    playlink,
                    '--input-conf=%s' % tmp_input_file,
                    '--title=%s' % playtitle
                ] + mpv_args
            , shell=False)

            p.communicate()

            # Reset next before it is too late
            next = 0

            if p.returncode == 0 or p.returncode == 4:
                # Good or hard exit ('>' or 'q').
                # Add to seen and remove from next print.
                seen_links.append(link)
                save_links.remove(link)
                print("")
                continue
            elif p.returncode == 5:
                # Exit but keeping the video for later. ('Ctrl+h')
                print("Reddytt: Hold-on exit detected.")
            elif p.returncode == 6:
                # Exit but download the video. ('Ctrl+d')
                print("Reddytt: Downloading.")
                p = subprocess.Popen(
                    [
                        'youtube-dl',
                        link[0],
                        "-o",
                        dl_dir + "/%(title)s.%(ext)s"
                    ] + ydl_args
                , shell=False)
                p.communicate()
                # And store and proceed to the next.
                seen_links.append(link)
                save_links.remove(link)
                print("")
                continue
            elif p.returncode == 7:
                # Exit and exclude the video ('Ctrl+x')
                print("Reddytt: Excluding video in the future.\n")
                # Continue as with 0 exit code, but append to exclude
                seen_links.append(link)
                save_links.remove(link)
                exclude_links.append(link[0])
                continue
            else:
                # Something else happened. Bad link perhaps.
                print("Reddytt: mpv exited in an unexpected way. Exit code: ", p.returncode)
                print("")
                seen_links.append(link)
                save_links.remove(link)
                continue

        print("Reddytt: Saving state to files.")
        with open(seen_file, 'wb') as f:
            pickle.dump(seen_links, f)
        with open(unseen_file, 'wb') as f:
            tosave_links = excluded_links + save_links
            pickle.dump(tosave_links, f)
        with open(exclude_file, 'wb') as f:
            pickle.dump(exclude_links, f)

        if test_mode:
            print("Reddytt: Cleaning up test mode.")
            rmtree(work_dir)

        print("Reddytt: Exiting. Bye!")
        # Exiting interactive mode
        sys.exit()

    # Normal mode play

    for link in watch_links:

        # Verify integrety of `link` variable, this is to avoid bug that can appear using files generated from reddytt older than v1.2
        if not type(link) == tuple:
            link = (link, '')

        if (link[0] in map(lambda x: x[0], seen_links)) and not radio_mode:
            print("Reddytt: Link seen. Skipping.")
            # Link is seen, do not need to save.
            save_links.remove(link)
            print("Reddytt: Links left: %i" % len(save_links))
        else:
            tmp_input(work_dir, link, len(save_links))

            not_done = True
            loop_counter = 0
            while not_done and loop_counter < 10:
                loop_counter += 1

                print("\nReddytt: Title: %s\n" % link[1])
                if download_mode:
                    p = subprocess.Popen(
                        [
                            'youtube-dl',
                            link[0],
                            "-o",
                            dl_dir + "/%(title)s.%(ext)s"
                        ] + ydl_args
                    , shell=False)
                else:
                    # Prepare title and link
                    playtitle = link[1].replace('"',"'")
                    playlink = link[0]

                    if len(length) > 0:
                        if not all((lambda l: (length[0] < l, length[-1] > l))(video_time(playlink))):
                            print("Reddytt: Video not in duration length, skipping.")
                            not_done = False
                            p = dummy_process(0)
                            continue

                    # Play
                    p = subprocess.Popen(
                        [
                            'mpv',
                            playlink,
                            '--input-conf=%s' % tmp_input_file,
                            '--title=%s' % playtitle
                        ] + mpv_args
                    , shell=False)


                p.communicate()
                not_done = p.returncode == 2

            os.system("rm {}".format(tmp_input_file))
            # Separate mpv and reddytt output
            print("")
            # Print the link (useful for saving manually if mpv messed up output)
            print("Reddytt: That was: %s" % link[1])
            print("Reddytt:           %s\n" % link[0])
            if p.returncode == 0:
                # The video finished or you hit '>' (default reddytt binding), this is a good exit.
                # Store the video in seen_links.
                seen_links.append(link)
                save_links.remove(link)
                # Print some stats
                print("Reddytt: Links left: %i" % len(save_links))
                # New line to separate next mpv output
                print("")
            elif p.returncode == 4:
                # You made a hard exit, and want to stop. ('q')
                # Store the links and exit the program.
                print("Reddytt: Forced exit detected. Saving and exiting.")
                with open(seen_file, 'wb') as f:
                    pickle.dump(seen_links, f)
                with open(unseen_file, 'wb') as f:
                    tosave_links = excluded_links + save_links
                    pickle.dump(tosave_links, f)
                with open(exclude_file, 'wb') as f:
                    pickle.dump(exclude_links, f)
                # Exit program.
                if test_mode:
                    rmtree(work_dir)
                sys.exit()
            elif p.returncode == 5:
                # Exit but keeping the video for later. ('Ctrl+h')
                print("Reddytt: Hold-on exit detected.")
                # Nothing needs to be done. We want the link kept in `save_links`
                # so that it is saved in the `unseen` file and comes back the next time.
            elif p.returncode == 6:
                # Exit but download the video. ('Ctrl+d')
                print("Reddytt: Downloading.")
                p = subprocess.Popen(
                    [
                        'youtube-dl',
                        link[0],
                        "-o",
                        dl_dir + "/%(title)s.%(ext)s"
                    ] + ydl_args
                , shell=False)
                p.communicate()
                # And store and proceed to the next.
                seen_links.append(link)
                save_links.remove(link)
            elif p.returncode == 7:
                # Exit and exclude the video ('Ctrl+x')
                print("Reddytt: Excluding video in the future.")
                # Continue as with 0 exit code, but append to exclude
                seen_links.append(link)
                save_links.remove(link)
                exclude_links.append(link[0])
                # Print some stats
                print("Reddytt: Links left: %i" % len(save_links))
                # New line to separate next mpv output
                print("")
            else:
                # Something else happened. Bad link perhaps.
                print("Reddytt: mpv exited in an unexpected way. Exit code: ", p.returncode)
                # New line to separate next mpv output
                print("")
                # Store in seen_links to avoid in the future.
                seen_links.append(link)
                save_links.remove(link)

    print("Reddytt: All links consumed. Saving and exiting.")
    # The playlist is finished. Save everything.
    with open(seen_file, 'wb') as f:
        pickle.dump(seen_links, f)
    with open(unseen_file, 'wb') as f:
        tosave_links = excluded_links + save_links
        pickle.dump(tosave_links, f)
    with open(exclude_file, 'wb') as f:
        pickle.dump(exclude_links, f)
    if test_mode:
        rmtree(work_dir)
